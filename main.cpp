#include <iostream>
#include <thread>
#include <chrono>

#include "window.h"
#include "texture.h"
#include "tile.h"
#include "helpers.h"

using namespace std;
Texture myTexture;
//
//Tile pm (0,0, { { 1,1}, {1,2}, {1,1}, {1,3}},1,Pacman,1,1) ;
vector<Tile> testTiles = getTestTiles();

int main()
{
    // SpriteSheet Filename
    string spriteFilename = SPRITEFILENAME; // Leave this line
    myTexture.loadFile(spriteFilename, 20, 20);

    // Setup and Load Texture object h
    bool quit = false;
    int frame = 0;
    Tile pm (0,0,{{1,1},{1,2},{1,1},{1,3}},Pacman,1,1);
    
    while(!quit){
        // Handle any SDL Events
        SDL_Event e;
        while (SDL_PollEvent(&e)){
              if(e.type == SDL_QUIT){
                     quit = true;
              }
              
        }
        
        SDL_SetRenderDrawColor(myTexture.myWin.sdlRenderer, 0, 0, 0, 250);
        SDL_RenderClear(myTexture.myWin.sdlRenderer);
        // Draw the current state to the screen.
        
        for (int i = 0; i < testTiles.size(); i++)
          {
          testTiles[i].render(&myTexture,frame);
        }
        
        
        SDL_RenderPresent(myTexture.myWin.sdlRenderer);
    
        frame++;

        // Such as resize, clicking the close button,
         //  and process and key press events.

    this_thread::sleep_for(chrono::milliseconds(75));
    
    }
    return 0;

}
